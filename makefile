bin/script: obj/main.o obj/funciones_cripto.o obj/cifrado_autollave.o
	gcc -Wall $^ -o $@

obj/main.o: src/main.c
	gcc -Wall -I include/ $< -c -o $@ 

obj/funciones_cripto.o: src/funciones_cripto.c
	gcc -Wall -I include/ $< -c -o $@

obj/cifrado_autollave.o: src/cifrado_autollave.c
	gcc -Wall -I include/ $< -c -o $@

clean:
	rm obj/*o
run:
	./bin/script
