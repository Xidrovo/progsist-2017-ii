#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

char arregloDeLetras[26]={'A', 	'B','C','D','E','F','G','H','I','J','K',
					'L','M','N','O','P','Q','R','S','T','U',
					'V','W','X','Y','Z'};

//Esta función me retornará el número que equivale la letra a ingresar.
//La función asumirá que la letra ingresada está entre A-Z.
int CharPosition(char letra){
	int i = 0;
	while ( (char)toupper((int)letra) != arregloDeLetras[i] ){ i++;	}
	return i;
}

char* cifrado_autollave(char* mensaje, char* contrasena){

	int index = 0, indexPass = 0, nuevoValor = 0;
	char NuevoMsj[100];
	strcat(contrasena,mensaje);
	while(1){
	//Si el mensaje llega a su fin, se da el fin del lazo while.
		if(mensaje[index] == '\0')	{	break;		}
	//Ignoro los espacios para no modificarlos.
		if(mensaje[index] == ' ')	{	NuevoMsj[index] = ' '; index++; 	}
		if(contrasena[indexPass] == ' '){	indexPass++;	}
	//En esta parte cifro la letra con la autollave.
		nuevoValor = CharPosition( mensaje[index] ) + CharPosition( contrasena[indexPass] );
		nuevoValor = nuevoValor % 26;
	//Apendizo la nueva letra cifrada al String a retornar.
		NuevoMsj[index] = arregloDeLetras[nuevoValor];
		index++;
		indexPass++;
	}
	strcpy(mensaje, NuevoMsj);
	return mensaje;
}
