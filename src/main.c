#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funciones_cripto.h"

#define MAX 100
/*
	Xavier Idrovo
	Hector Carvajal
*/
int main(int argc, char *argv[]){
	char nombre[MAX], nombreCpy[MAX], pass[MAX]; 
	long avance = 3;
	if(argc == 4){
		strcpy(nombre, argv[2]);
		strcpy(nombreCpy, argv[2]);
		strcpy(pass, argv[3]);
		avance = strtol(argv[1], NULL, 10);
		printf("%d\n", (int)avance);
		cifrado_ciclico(nombre,(int)avance);
		printf("Ciclico:\t\t%s\n", nombre);
		printf("Codigo morse: \t"); stringToMorse(nombre);
		cifrado_autollave(nombreCpy, pass);
		printf("\ncifrado autollave: \t%s \n", nombreCpy);
		printf("\n");
	}
	else if(argc == 3){
		strcpy(nombre, argv[2]);
		strcpy(nombreCpy, argv[2]);
		strcpy(pass, "MARAZUL");
		avance = strtol(argv[1], NULL, 10);
		printf("%d\n", (int)avance);
		cifrado_ciclico(nombre,(int)avance);
		printf("Ciclico:\t\t%s\n", nombre);
		printf("Codigo morse: \t"); stringToMorse(nombre);
		cifrado_autollave(nombreCpy, pass);
		printf("\ncifrado autollave: \t%s \n", nombreCpy);
		printf("\n");
	}
	else{		
		strcpy(nombre, "Hola Pedro");
		strcpy(nombreCpy, "Hola Pedro");
		strcpy(pass, "MARAZUL");
		cifrado_ciclico(nombre,(int)avance);
		printf("Ciclico:\t\t%s\n", nombre);
		printf("Codigo morse: \t"); stringToMorse(nombre);
		cifrado_autollave(nombreCpy, pass);
		printf("\ncifrado autollave: \t%s \n", nombreCpy);
		printf("\n");
		
	}
	return 0;
}
