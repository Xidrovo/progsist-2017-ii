#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funciones_cripto.h"

#define MAX 100
/*
Xavier Idrovo
*/
int main(int argc, char *argv[]){
	char nombre[MAX]; 
	long avance;
	if(argc == 3){
		strcpy(nombre, argv[2]);
		avance = strtol(argv[1], NULL, 10);
		printf("%d\n", (int)avance);
		cifrado_ciclico(nombre,(int)avance);
		printf("Ciclico:\t%s\n", nombre);
		printf("Codigo morse: \t"); stringToMorse(nombre);
		printf("\n");
	}
	else{		
		strcpy(nombre, "Hola mundo");
		cifrado_ciclico(nombre,(int)avance);
		printf("Ciclico:\t%s\n", nombre);
		printf("Codigo morse: \t"); stringToMorse(nombre);
		printf("\n");
		
	}
	return 0;
}
